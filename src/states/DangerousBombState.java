package states;

import game.Game;

import javax.swing.*;


public class DangerousBombState implements GameObjectState {


    @Override
    public void slice() {
        int lives = Game.getInstance().getLives();
        lives--;
        Game.getInstance().setLives(lives);
    }

    @Override
    public ImageIcon[] objectState() {
        return null;
    }
}
