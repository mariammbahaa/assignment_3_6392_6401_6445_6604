package states;

import game.Game;

import javax.swing.*;

public interface GameObjectState {

    public void slice();
    public ImageIcon[] objectState();
}
