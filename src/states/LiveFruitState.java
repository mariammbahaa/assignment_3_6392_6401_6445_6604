package states;

import game.Game;

import javax.swing.*;

public class LiveFruitState implements GameObjectState {
    @Override
    public void slice() {
        Game.getInstance().sliceObjects();
        int livesCount = Game.getInstance().getLives();
        if (livesCount < 3) {
            Game.getInstance().setLives(livesCount++);
        }
        Game.getInstance().bestScoreComparator();
    }

    @Override
    public ImageIcon[] objectState() {
        ImageIcon[] images = new ImageIcon[2];
        images[0] = new ImageIcon("src//images//liveFruitCut1.png");
        images[1] = new ImageIcon("src//images//liveFruitCut2.png");
        return images;
    }
}
