package states;

import game.Game;

import javax.swing.*;

public class BonusFruitState implements GameObjectState {
    @Override
    public void slice() {
        int currentScore = Game.getInstance().getCurrentScore();
        currentScore +=100;
        Game.getInstance().sliceObjects();
        Game.getInstance().bestScoreComparator();
        Game.getInstance().setCurrentScore(currentScore);
    }

    @Override
    public ImageIcon[] objectState() {
        ImageIcon[] images = new ImageIcon[2];
        images[0] = new ImageIcon("src//images//bonusFruitCut1.png");
        images[1] = new ImageIcon("src//images//bonusFruitCut2.png");
        return images;
    }
}
