package game;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "level")
@XmlAccessorType(XmlAccessType.FIELD)
public class Level {

    @XmlElement(name = "maxScore")
    private int maxScore;
    @XmlElement(name = "fruitBound")
    private int fruitBound;
    @XmlElement(name = "bombBound")
    private int bombBound;

    public int getMaxNumberOfFruits() {
        return fruitBound;
    }
    public int getMaxNumberOfBombs() {
        return bombBound;
    }
    public int getMaxScore() {
        return maxScore;
    }
}
