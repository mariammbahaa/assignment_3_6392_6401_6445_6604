package game;

import gameObjects.GameObject;
import gameObjects.factory.Factory;
import gameObjects.factory.BombFactory;
import gameObjects.factory.FruitFactory;
import observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Game implements GameActions {

    private static Game game = null;
    private int level;
    private int currentScore;
    private int bestScore;
    private int lives;
    private int fruitBound;
    private int bombBound;
    private GameState gameState;
    List<Observer> myObservers = new ArrayList<>();
    Factory bombFactory = new BombFactory();
    Factory fruitFactory = new FruitFactory();
    //Levels levels = null;
    List<Level> levels = null;

    public void attach(Observer observer) {
        myObservers.add(observer);
    }

    public void notifyAllObservers() {
        for (Observer observer : myObservers) {
            observer.update();
        }
    }

    private Game()
    {
        level = 1;
        currentScore = 0;
        lives = 3;
        levels = new ArrayList<>();
    }

    public static Game getInstance()
    {
        if(game == null)
        {
            game = new Game();
        }
        return game;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public int getBestScore() {
        return bestScore;
    }

    public void setBestScore(int bestScore) {
        this.bestScore = bestScore;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getFruitBound() {
        return fruitBound;
    }

    public void setFruitBound(int fruitBound) {
        this.fruitBound = fruitBound;
    }

    public int getBombBound() {
        return bombBound;
    }

    public void setBombBound(int bombBound) {
        this.bombBound = bombBound;
    }


    @Override
    public List<GameObject> createGameObjects(int c) {

        List<GameObject> gameObjects = new ArrayList<>();
        Random r = new Random();
        int numberOfBombs = r.nextInt(bombBound) + 1;
        int xLocation = 400;
        int yLocation = 500;
        for (int i = 0; i < numberOfBombs; i++) {
            GameObject g = createBomb();
            g.setXLocation(xLocation);
            g.setYLocation(yLocation);
            gameObjects.add(g);
            xLocation += 80;
        }
        int numberOfFruits = r.nextInt(fruitBound )+1;
        for (int i = 0; i < numberOfFruits; i++) {
            GameObject g = createFruit(c);
            g.setXLocation(xLocation);
            g.setYLocation(yLocation);
            gameObjects.add(g);
            xLocation += 80;
        }
        return gameObjects;
    }

    public GameObject createBomb(){

        int random1 = createRandom().nextInt(2);
        if (random1 == 0) {
            return bombFactory.getObject("fatal");
        } else {
            return bombFactory.getObject("dangerous");
        }
    }

    public GameObject createFruit(int c)
    {
        int random2 = createRandom().nextInt(6);
        if (random2 == 0) {
            return fruitFactory.getObject("apple");
        } else if(random2 == 1){
            return fruitFactory.getObject("pineapple");
        }
        else if(random2 == 2 && c > 300 && c%5 ==0) {
            return fruitFactory.getObject("tutti frutti");
        }
        else if(random2 ==3 && c > 300 && c%3 == 0)
        {
            return fruitFactory.getObject("live");
        }
        else if(random2 == 4 && c>200 && c%2 ==0)
        {
            return fruitFactory.getObject("bonus");
        }
        else {
            return fruitFactory.getObject("pineapple");
        }
    }

    public Random createRandom()
    {
        Random random = new Random();
        return random;
    }

    @Override
    public void updateObjectsLocations() {
    }

    @Override
    public void sliceObjects() {
        if (currentScore >=  levels.get(level-1).getMaxScore()) {
            levelUp();
        }
    }

    @Override
    public void levelUp() {
        if(level< 3)
        {
        level ++;
        lives = 3;
        fruitBound = levels.get(level-1).getMaxNumberOfFruits();
        bombBound = levels.get(level-1).getMaxNumberOfBombs();
        }
    }

    @Override
    public GameState saveGame() {
        gameState = new GameState(this.level,this.currentScore,this.bestScore,this.lives);
        return gameState;
    }

    @Override
    public void loadGame(GameState oldState, List<Level> levels) {
            this.level = oldState.getLevel();
            this.currentScore = oldState.getCurrentScore();
            this.bestScore = oldState.getBestScore();
            //this.lives = oldState.getLives();
            this.levels = levels;
            fruitBound = levels.get(level-1).getMaxNumberOfFruits();
            bombBound = levels.get(level-1).getMaxNumberOfBombs();
    }

    @Override
    public void resetGame() {
        level = 1;
        currentScore = 0;
        bestScore = CareTaker.getInstance().getStateStack().getBestScore();
        lives = 3;
    }

    public void bestScoreComparator()
    {
        if(CareTaker.getInstance().getGameStateStack().empty())
        {
            setBestScore(currentScore);
        }
        else {
            setBestScore(CareTaker.getInstance().getStateStack().getBestScore());
        }
            if(bestScore < currentScore)
                setBestScore(currentScore);
    }
}
