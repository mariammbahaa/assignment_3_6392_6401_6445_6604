import javazoom.jl.decoder.JavaLayerException;
import sounds.CuttingSound;

import javax.swing.*;
import javax.xml.bind.JAXBException;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;

public class HomeScreenMouseMotionListener implements MouseMotionListener {

    JLabel watermelonLabel;
    Mainvieww mainvieww;
    GameController gameController;

    public HomeScreenMouseMotionListener(JLabel label, Mainvieww mainvieww)
    {
        gameController = GameController.getInstance();
        watermelonLabel = label;
        this.mainvieww = mainvieww;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        try {
            gameController.loadGame();
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        try {
            gameController.resetGame();
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        try {
            CuttingSound.playSound();
        } catch (JavaLayerException ex) {
            ex.printStackTrace();
        }
        ImageIcon imageIcon = new ImageIcon("src//images//watermelonCut1.png");
        watermelonLabel.setIcon(imageIcon);
        mainvieww.changePanelGamePanel();
        CuttingSound.stopSound();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
