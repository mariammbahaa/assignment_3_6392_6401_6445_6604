package gameObjects;

import game.Game;
import states.AppleState;
import states.LiveFruitState;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class LiveFruit extends Fruit {

    public LiveFruit() {
        super();
        objectState = new LiveFruitState();
        initialVelocity = 30;
        fallingVelocity = 35;
    }

    @Override
    public void moveUp() {
        Random r = new Random();
        int m = r.nextInt(3)+5;
        setYLocation(YLocation - m);
        setXLocation(XLocation - m);
        if (YLocation < 20) {
            setMovingUp(false);
        }
    }

    @Override
    public void moveDown() {
        setYLocation(YLocation + getFallingVelocity());
        if ( YLocation > 600 || XLocation < 0) {
            setHasMovedOffScreen(true);
        }
    }

    @Override
    public void slice(Game game) {
        isSliced = true;
        objectState.slice();
    }

    @Override
    public void move(double time) {

    }

    @Override
    public Image[] getImages() {
        ImageIcon icon = new ImageIcon("src\\images\\liveFruit.png");
        images[0] = icon.getImage();
        return images;
    }
}
