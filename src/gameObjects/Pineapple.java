package gameObjects;

import game.Game;
import states.PineappleState;

import javax.swing.*;
import java.awt.*;
import java.util.Random;


public class Pineapple extends Fruit {

    public Pineapple() {
        super();
        objectState = new PineappleState();
        initialVelocity = 30;
        fallingVelocity = 20;
    }

    @Override
    public void moveUp() {
        Random r = new Random();
        int m = r.nextInt(5);
        setYLocation(YLocation - m);
        setXLocation(XLocation - m);
        if (YLocation < 20) {
            setMovingUp(false);
        }
    }

    @Override
    public void moveDown() {
        setYLocation(YLocation + getFallingVelocity());
        if ( YLocation > 600 || XLocation < 0) {
            setHasMovedOffScreen(true);
        }
    }

    @Override
    public void slice(Game game) {
        isSliced = true;
        objectState.slice();
    }

    @Override
    public void move(double time) {
    }

    @Override
    public Image[] getImages() {
        ImageIcon icon = new ImageIcon("src\\images\\pineapple.png");
        images[0] = icon.getImage();
        return images;
    }
}
