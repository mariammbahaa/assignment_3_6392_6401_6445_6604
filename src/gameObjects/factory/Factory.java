package gameObjects.factory;

import gameObjects.GameObject;

public interface Factory {
    public GameObject getObject(String objectName);
}
