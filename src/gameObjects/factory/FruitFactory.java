package gameObjects.factory;

import gameObjects.*;

public class FruitFactory implements Factory {
    @Override
    public GameObject getObject(String objectName) {
        if(objectName.equalsIgnoreCase("Apple"))
            return new Apple();
        else if(objectName.equalsIgnoreCase("Pineapple"))
            return new Pineapple();
        else if(objectName.equalsIgnoreCase("Watermelon"))
            return new Watermelon();
        else if( objectName.equalsIgnoreCase("Tutti frutti"))
            return new TuttiFruiti();
        else if( objectName.equalsIgnoreCase("Bonus"))
            return new BonusFruit();
        else
            return  new LiveFruit();
    }
}
