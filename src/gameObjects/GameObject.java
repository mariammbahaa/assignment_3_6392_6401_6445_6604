package gameObjects;

import game.Game;
import states.GameObjectState;

import java.awt.*;


public abstract class GameObject {

    GameObjectState objectState;
    int XLocation;
    int YLocation;
    int maxHeight;
    int initialVelocity;
    int fallingVelocity;
    Boolean isSliced;
    Boolean hasMovedOffScreen;
    Boolean movingUp;

    public int getXLocation() {
        return XLocation;
    }

    public void setXLocation(int XLocation) {
        this.XLocation = XLocation;
    }

    public int getYLocation() {
        return YLocation;
    }

    public void setYLocation(int YLocation) {
        this.YLocation = YLocation;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    public int getInitialVelocity() {
        return initialVelocity;
    }

    public void setInitialVelocity(int initialVelocity) {
        this.initialVelocity = initialVelocity;
    }

    public int getFallingVelocity() {
        return fallingVelocity;
    }

    public void setFallingVelocity(int fallingVelocity) {
        this.fallingVelocity = fallingVelocity;
    }

    public Boolean getSliced() {
        return isSliced;
    }

    public void setSliced(Boolean sliced) {
        isSliced = sliced;
    }

    public Boolean getHasMovedOffScreen() {
        return hasMovedOffScreen;
    }

    public void setHasMovedOffScreen(Boolean hasMovedOffScreen) {
        this.hasMovedOffScreen = hasMovedOffScreen;
    }

    public Boolean getMovingUp() {
        return movingUp;
    }

    public void setMovingUp(Boolean movingUp) {
        this.movingUp = movingUp;
    }

    public abstract void moveUp();

    public abstract void moveDown();


    public abstract void slice(Game game);
    public abstract void move(double time);
    public abstract Image[] getImages();

}
