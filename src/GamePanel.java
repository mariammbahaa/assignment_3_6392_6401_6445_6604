import controller.FontClass;
import gameObjects.*;
import states.*;

import javax.swing.*;
import javax.xml.bind.JAXBException;


import java.awt.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GamePanel extends javax.swing.JPanel {

    GameController gameController;
    int c;
    JLabel levelLabel;
    JLabel currentScoreLabel;
    JLabel bestScoreLabel;
    JLabel lives_label1;
    JLabel lives_label2;
    JLabel lives_label3;
    JLabel timeLabel;
    java.util.List<GameObject> gameObjects;
    Timer timer;
    Mainvieww mainvieww;
    static long milliseconds = 0;
    static int seconds = 0;
    static int minutes = 0;
    static boolean state = true;

    public GamePanel(Mainvieww mainvieww) throws IOException, FontFormatException {
        gameController = GameController.getInstance();
        this.mainvieww = mainvieww;
        this.setLayout(new BorderLayout());
        this.setLayout(null);
        setPreferredSize(new Dimension(1000, 600));
        TimerActionListener timerActionListener = new TimerActionListener(this);
        timer = new Timer(20, timerActionListener);
        timer.start();
        ImageIcon cursorImageIcon = new ImageIcon("src//images//knife1.png");
        Image cursorImage = cursorImageIcon.getImage();
        Image cursorImage2 = cursorImage.getScaledInstance(1000, 1000,  java.awt.Image.SCALE_SMOOTH);
        Cursor customCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage2, new Point(0, 0), "customCursor");
        setCursor(customCursor);

        lives_label1 = new JLabel();
        lives_label2 = new JLabel();
        lives_label3 = new JLabel();
        levelLabel = new JLabel();
        currentScoreLabel = new JLabel();
        bestScoreLabel = new JLabel();
        timeLabel = new JLabel();

        lives_label1.setBounds(850,20,50,50);
        lives_label1.setOpaque(false);
        lives_label2.setBounds(900,20,50,50);
        lives_label2.setOpaque(false);
        lives_label3.setBounds(950,20,50,50);
        lives_label3.setOpaque(false);
        ImageIcon life = new ImageIcon("src//images//X.png");
        lives_label1.setIcon(life);
        lives_label2.setIcon(life);
        lives_label3.setIcon(life);

        levelLabel.setBounds(680,5, 200,100);
        levelLabel.setFont(FontClass.getGameFont());
        levelLabel.setForeground(Color.white);

        currentScoreLabel.setBounds(400,5, 300,100);
        currentScoreLabel.setFont(FontClass.getGameFont());
        currentScoreLabel.setForeground(Color.white);

        bestScoreLabel.setBounds(180,5, 300,100);
        bestScoreLabel.setFont(FontClass.getGameFont());
        bestScoreLabel.setForeground(Color.white);

        gameObjects = gameController.createGameObjects(c);

        MyMouseMotionListener mouseMotionListener = new MyMouseMotionListener(this, mainvieww);
        addMouseMotionListener(mouseMotionListener);
    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        ImageIcon imageIcon = new ImageIcon("src//images//background.jpg");
        Image image = imageIcon.getImage();
        g.drawImage(image, 0, 0, null);
        timeLabel.setOpaque(false);
        timeLabel.setForeground(Color.white);
        try {
            timeLabel.setFont(FontClass.getGameFont());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FontFormatException e) {
            e.printStackTrace();
        }
        try {
            g.setFont(FontClass.getGameFont());
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        g.setColor(Color.WHITE);

        timeLabel.setBounds(0,10,100,100);

        Thread t = new Thread(){
            public void run(){
                for(;;)
                {
                    if (state==true)
                    {

                        try {
                            sleep(1);
                            if(milliseconds>1000)
                            {
                                milliseconds=0;
                                seconds++;
                            }
                            if(seconds>60)
                            {
                                milliseconds=0;
                                seconds=0;
                                minutes++;
                            }
                            //timeLabel.setText(minutes + " : "+ seconds);
                            milliseconds++;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(TimerActionListener.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        };
        t.start();

        this.add(lives_label1);
        this.add(lives_label2);
        this.add(lives_label3);
        this.add(timeLabel);
        this.add(levelLabel);

        currentScoreLabel.setText("Current Score  " + gameController.getCurrentScore());
        currentScoreLabel.setVisible(true);
        this.add(currentScoreLabel);

        bestScoreLabel.setText("High Score  " + gameController.getBestScore());
        bestScoreLabel.setVisible(true);
        this.add(bestScoreLabel);


        for (int i = 0; i < gameObjects.size(); i++) {
            GameObject gameObject = gameObjects.get(i);
            if (!gameObject.getSliced()) {
                g.drawImage(gameObject.getImages()[0], gameObject.getXLocation(), gameObject.getYLocation(), 70, 70, this);
            } else {
                if (gameObject instanceof Pineapple) {
                    GameObjectState state = new PineappleState();
                    ImageIcon[] images = state.objectState();
                    g.drawImage(images[1].getImage(), gameObject.getXLocation() - 50, gameObject.getYLocation() - 50, 70, 70, this);
                    g.drawImage(images[0].getImage(), gameObject.getXLocation(), gameObject.getYLocation(), 70, 70, this);
                }
                if (gameObject instanceof Apple) {
                    GameObjectState state = new AppleState();
                    ImageIcon[] images = state.objectState();
                    g.drawImage(images[1].getImage(), gameObject.getXLocation() - 50, gameObject.getYLocation() - 50, 70, 70, this);
                    g.drawImage(images[0].getImage(), gameObject.getXLocation(), gameObject.getYLocation(), 70, 70, this);
                }
                if (gameObject instanceof Watermelon) {
                    GameObjectState state = new WatermelonState();
                    ImageIcon[] images = state.objectState();
                    g.drawImage(images[1].getImage(), gameObject.getXLocation() - 50, gameObject.getYLocation() - 50, 70, 70, this);
                    g.drawImage(images[0].getImage(), gameObject.getXLocation(), gameObject.getYLocation(), 70, 70, this);
                }
                if (gameObject instanceof TuttiFruiti) {
                    GameObjectState state = new TuttiFrutiState();
                    ImageIcon[] images = state.objectState();
                    g.drawImage(images[1].getImage(), gameObject.getXLocation() - 50, gameObject.getYLocation() - 50, 70, 70, this);
                    g.drawImage(images[0].getImage(), gameObject.getXLocation(), gameObject.getYLocation(), 70, 70, this);
                }
                if (gameObject instanceof BonusFruit) {
                    GameObjectState state = new BonusFruitState();
                    ImageIcon[] images = state.objectState();
                    g.drawImage(images[1].getImage(), gameObject.getXLocation() - 50, gameObject.getYLocation() - 50, 70, 70, this);
                    g.drawImage(images[0].getImage(), gameObject.getXLocation(), gameObject.getYLocation(), 70, 70, this);
                }
                if (gameObject instanceof LiveFruit) {
                    GameObjectState state = new LiveFruitState();
                    ImageIcon[] images = state.objectState();
                    g.drawImage(images[1].getImage(), gameObject.getXLocation() - 50, gameObject.getYLocation() - 50, 70, 70, this);
                    g.drawImage(images[0].getImage(), gameObject.getXLocation(), gameObject.getYLocation(), 70, 70, this);
                }
            }
            repaint();
        }
    }
}
