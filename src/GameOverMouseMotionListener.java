import javazoom.jl.decoder.JavaLayerException;
import sounds.CuttingSound;

import javax.swing.*;
import javax.xml.bind.JAXBException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class GameOverMouseMotionListener implements MouseMotionListener {

    JLabel label;
    Mainvieww mainvieww;
    GameController gameController;

    public GameOverMouseMotionListener(JLabel label, Mainvieww mainvieww){
        this.label = label;
        this.mainvieww = mainvieww;
        gameController = GameController.getInstance();
    }

    @Override
    public void mouseDragged(MouseEvent e) {

        try {
            CuttingSound.playSound();
        } catch (JavaLayerException ex) {
            ex.printStackTrace();
        }
        ImageIcon imageIcon = new ImageIcon("src//images//watermelonCut1.png");
        label.setIcon(imageIcon);
        CuttingSound.stopSound();
        try {
            gameController.saveGame();
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        try {
            gameController.resetGame();
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        mainvieww.changePanelHomescreen();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
