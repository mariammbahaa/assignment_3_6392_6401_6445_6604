package commands;

import game.Game;

import javax.xml.bind.JAXBException;

public interface Command {
    public void execute() throws JAXBException;
}
