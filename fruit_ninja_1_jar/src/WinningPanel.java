import controller.FontClass;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class WinningPanel extends JPanel {

    JLabel winningLabel;
    Mainvieww mainvieww;

    public WinningPanel(Mainvieww mainvieww) throws IOException, FontFormatException {

        this.mainvieww = mainvieww;
        winningLabel = new JLabel();
        setLayout(new BorderLayout());
        winningLabel.setOpaque(false);
        ImageIcon imageIcon = new ImageIcon("src//images//watermelon.png");
        Image image = imageIcon.getImage();
        Image newImage = image.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH);
        imageIcon = new ImageIcon(newImage);
        winningLabel.setIcon(imageIcon);
        setPreferredSize(new Dimension(1000,600));
        winningLabel.setText("Play again");
        winningLabel.setSize(winningLabel.getPreferredSize());
        winningLabel.setFont(FontClass.getGameFont());
        winningLabel.setForeground(Color.white);
        winningLabel.addMouseMotionListener(new GameOverMouseMotionListener(winningLabel,mainvieww));
    }

    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        ImageIcon imageIcon = new ImageIcon("src//images//background.jpg");
        Image image = imageIcon.getImage();
        this.add(winningLabel,BorderLayout.SOUTH);
        g.drawImage(image, 0,0 ,null);
        g.setFont(FontClass.getGameOverFont());
        g.setColor(Color.white);
        g.drawString("YOU WON!",300,300);
        winningLabel.setVisible(true);
        this.validate();
        repaint();
    }
}
