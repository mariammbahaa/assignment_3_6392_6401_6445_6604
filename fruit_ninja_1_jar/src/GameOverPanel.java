import controller.FontClass;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class GameOverPanel extends JPanel {

    JLabel gameOverLabel;
    Mainvieww mainvieww;

    public GameOverPanel(Mainvieww mainvieww) throws IOException, FontFormatException {
        this.mainvieww = mainvieww;
        gameOverLabel = new JLabel();
        setLayout(new BorderLayout());
        gameOverLabel.setOpaque(false);
        ImageIcon imageIcon = new ImageIcon("src//images//watermelon.png");
        Image image = imageIcon.getImage();
        Image newImage = image.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH);
        imageIcon = new ImageIcon(newImage);
        gameOverLabel.setIcon(imageIcon);
        setPreferredSize(new Dimension(1000,600));
        gameOverLabel.setText("Play again");
        gameOverLabel.setSize(gameOverLabel.getPreferredSize());
        gameOverLabel.setFont(FontClass.getGameFont());
        gameOverLabel.setForeground(Color.white);
        gameOverLabel.addMouseMotionListener(new GameOverMouseMotionListener(gameOverLabel,mainvieww));

    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ImageIcon imageIcon = new ImageIcon("src//images//background.jpg");
        Image image = imageIcon.getImage();
        this.add(gameOverLabel,BorderLayout.SOUTH);
        g.drawImage(image, 0,0 ,null);
        g.setFont(FontClass.getGameOverFont());
        g.setColor(Color.white);
        g.drawString("Game over",300,300);
        gameOverLabel.setVisible(true);
        this.validate();
        repaint();
    }
}
