import game.Game;
import gameObjects.Bomb;
import gameObjects.FatalBomb;
import gameObjects.Fruit;
import gameObjects.GameObject;
import javazoom.jl.decoder.JavaLayerException;
import sounds.CuttingSound;
import sounds.FatalBombSound;

import javax.swing.*;
import javax.xml.bind.JAXBException;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;

import static java.awt.font.TextAttribute.FONT;


public class MyMouseMotionListener implements MouseMotionListener {

    GameController gameController;
    GamePanel g;
    GameOverPanel p ;
    Mainvieww m;

    public MyMouseMotionListener(GamePanel panel, Mainvieww m) throws IOException, FontFormatException {
        gameController = GameController.getInstance();
        this.g = panel;
        this.m = m;
        m.gamePanel = panel;
        p = new GameOverPanel(m);
    }

    @Override
    public void mouseDragged(java.awt.event.MouseEvent e) {

        gameController.bestScoreComparator();
        int x = e.getX();
        int y = e.getY();
        int width = 0;
        int height = 0;
        ImageIcon[] images;

        for (GameObject object : g.gameObjects) {
            if(object instanceof Bomb){
                width = 60;
                height = 60;
            }
            else if(object instanceof Fruit)
            {
                width =50;
                height = 50;
            }

            if (x >= object.getXLocation() && x <= object.getXLocation() + width) {
                if (y >= object.getYLocation() && y <= object.getYLocation() + height) {
                    if (!object.getSliced()) {

                        object.setMovingUp(false);
                        object.slice(Game.getInstance());
                        gameController.bestScoreComparator();
                        gameController.setCurrentScore(Game.getInstance().getCurrentScore());

                        if(object.getClass().equals(Fruit.class))
                        {
                            try {
                                CuttingSound.playSound();
                            } catch (JavaLayerException ex) {
                                ex.printStackTrace();
                            }
                        }
                        if(object.getClass().equals(FatalBomb.class))
                        {
                            try {
                                gameController.saveGame();
                            } catch (JAXBException ex) {
                                ex.printStackTrace();
                            }
                            try {
                                gameController.resetGame();
                            } catch (JAXBException ex) {
                                ex.printStackTrace();
                            }
                            try {
                                FatalBombSound.playSound();
                            } catch (JavaLayerException ex) {
                                ex.printStackTrace();
                            }
                            try {
                                m.changePanelGameOver();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            } catch (FontFormatException ex) {
                                ex.printStackTrace();
                            }
                            FatalBombSound.stopSound();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
