package game;

public class Player {

        String name;
        int score;

        private static Player P = null;

        private Player(){
        }

        public static Player getInstance(){
            if(P == null){
                P= new Player();
            }
            return P;
        }
}
