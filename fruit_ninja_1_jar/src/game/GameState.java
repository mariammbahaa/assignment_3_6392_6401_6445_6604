package game;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "gameState")
@XmlAccessorType(XmlAccessType.FIELD)
public class GameState {

    @XmlElement(name = "level")
    private int level;
    @XmlElement(name = "currentScore")
    private int currentScore;
    @XmlElement(name = "bestScore")
    private int bestScore;
    //@XmlElement(name = "lives")
    @XmlTransient
    private int lives;

    public GameState()
    {}

    public GameState(int level, int currentScore, int bestScore, int lives)
    {
        this.level = level;
        this.currentScore = currentScore;
        this.bestScore = bestScore;
        this.lives = lives;
    }

    public int getLevel() {
        return level;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public int getBestScore() {
        return bestScore;
    }

    public int getLives() {
        return lives;
    }

}
