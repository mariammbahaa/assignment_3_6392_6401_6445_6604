package game;

import gameObjects.GameObject;

import java.util.ArrayList;
import java.util.List;

public interface GameActions {

    // generate random objects to be shown in one window and initial positions
    public List<GameObject> createGameObjects(int c);

    // generate new positions of objects while moving
    public void updateObjectsLocations();

    // slice objects and level up
    public void sliceObjects();

    public void levelUp();
    public GameState saveGame();
    public void loadGame(GameState oldState, List<Level> levels);
    public void resetGame();
}
