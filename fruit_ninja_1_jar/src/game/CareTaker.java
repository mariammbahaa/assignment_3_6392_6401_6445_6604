package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class CareTaker {

    public static CareTaker careTaker = null;

    public Stack<GameState> getGameStateStack() {
        return gameStateStack;
    }

    private Stack<GameState> gameStateStack;

    private CareTaker()
    {
        gameStateStack = new Stack<>();
    }

    public static CareTaker getInstance()
    {
        if(careTaker == null)
        {
            careTaker = new CareTaker();
        }
       return careTaker;
    }

    //using arraylist and index
    private List<GameState> gameStatesList = new ArrayList<>();
    public void addStateList(GameState newState)
    {
        gameStatesList.add(newState);
    }
    public GameState getStateList(int index)
    {
       return gameStatesList.get(index);
    }

    //using stack
    //private Stack<GameState> gameStateStack = new Stack<>();
    public void addStateStack(GameState newState)
    {
        addStateList(newState);
        gameStateStack.push(newState);
    }

    public GameState getStateStack()
    {
        if(!gameStateStack.empty())
        return gameStateStack.peek();
        else return new GameState(1,0,0,3);
    }

}
