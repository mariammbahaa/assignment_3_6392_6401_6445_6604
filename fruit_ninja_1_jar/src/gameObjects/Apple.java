package gameObjects;

import game.Game;
import states.AppleState;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Apple extends Fruit {

    public Apple() {
        super();
        objectState = new AppleState();
        initialVelocity = 20;
        fallingVelocity = 10;
    }

    @Override
    public void moveUp() {
        Random r = new Random();
        int m = r.nextInt(5);
        setYLocation(YLocation - m);
        setXLocation(XLocation - m);
        if (YLocation < 20) {
            setMovingUp(false);
        }
    }

    @Override
    public void moveDown() {
        setYLocation(YLocation + getFallingVelocity());
        if ( YLocation > 600 || XLocation < 0) {
            setHasMovedOffScreen(true);
        }
    }

    @Override
    public void slice(Game game) {
        isSliced = true;
        objectState.slice();
    }

    @Override
    public void move(double time) {
    }

    @Override
    public Image[] getImages() {
        ImageIcon icon = new ImageIcon("src\\images\\apple.png");
        images[0] = icon.getImage();
        return images;
    }
}
