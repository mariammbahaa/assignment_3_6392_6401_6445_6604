package gameObjects;


import game.Game;
import states.DangerousBombState;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class DangerousBomb extends Bomb {

    public DangerousBomb()
    {
        super();
        objectState = new DangerousBombState();
        this.initialVelocity = 20;
        this.fallingVelocity = 10;
    }

    @Override
    public void slice(Game game) {
        isSliced = true;
        objectState.slice();
    }

    @Override
    public void move(double time) {

    }

    @Override
    public void moveUp() {
        Random r = new Random();
        int m = r.nextInt(5);
        setYLocation(YLocation - m);
        setXLocation(XLocation - m);
        if (YLocation < 20) {
            setMovingUp(false);
        }
    }

    @Override
    public void moveDown() {
        setYLocation(YLocation + getFallingVelocity());
        if ( YLocation > 800) {
            setHasMovedOffScreen(true);
        }
    }

    @Override
    public Image[] getImages() {
        ImageIcon icon = new ImageIcon("src\\images\\dangerousBomb.png");
        images[0] = icon.getImage();
        return images;
    }

}
