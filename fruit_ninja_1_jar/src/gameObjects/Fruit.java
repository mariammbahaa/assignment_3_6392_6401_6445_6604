package gameObjects;

import javafx.scene.image.ImageView;
import states.GameObjectState;

import java.awt.*;

public abstract class Fruit extends GameObject {

        protected Image[] images;

        public Fruit()
        {
                images = new Image[3];
                isSliced = false;
                hasMovedOffScreen = false;
                movingUp = true;
                maxHeight = 500;
        }
}
