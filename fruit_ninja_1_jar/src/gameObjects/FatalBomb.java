package gameObjects;

import game.Game;
import states.FatalBombState;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class FatalBomb extends Bomb{

    public FatalBomb() {
        super();
        objectState = new FatalBombState();
        initialVelocity = 10;
        fallingVelocity = 5;
    }

    @Override
    public void slice(Game game) {
        isSliced = true;
        objectState.slice();
    }

    @Override
    public void move(double time) {

    }

    @Override
    public void moveUp() {
        Random r = new Random();
        int m = r.nextInt(5);
        setYLocation(YLocation - m);
        setXLocation(XLocation - m);
        if (YLocation < 20) {
            setMovingUp(false);
        }
    }

    @Override
    public void moveDown() {
        setYLocation(YLocation + getFallingVelocity());
        if ( YLocation > 800 || XLocation < 0) {
            setHasMovedOffScreen(true);
        }
    }

    @Override
    public Image[] getImages() {
        ImageIcon icon = new ImageIcon("src\\images\\fatalBomb.png");
        images[0] = icon.getImage();
        return images;
    }

}
