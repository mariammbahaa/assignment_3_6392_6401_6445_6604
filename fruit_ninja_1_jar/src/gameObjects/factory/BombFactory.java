package gameObjects.factory;

import gameObjects.DangerousBomb;
import gameObjects.FatalBomb;
import gameObjects.GameObject;

public class BombFactory implements Factory {

    @Override
    public GameObject getObject(String objectName) {
        if(objectName.equalsIgnoreCase("Fatal"))
            return new FatalBomb();
        else if(objectName.equalsIgnoreCase("Dangerous"))
            return new DangerousBomb();
        else
            return null;
    }
}
