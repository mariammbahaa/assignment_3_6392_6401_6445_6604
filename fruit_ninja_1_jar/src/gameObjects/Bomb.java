package gameObjects;
import java.awt.*;

public abstract class Bomb extends GameObject {

    protected Image[] images;

    public Bomb()
    {
        images = new Image[1];
        isSliced = false;
        hasMovedOffScreen = false;
        movingUp = true;
        maxHeight = 500;
    }
}
