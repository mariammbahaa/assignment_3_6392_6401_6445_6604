package gameObjects;

import game.Game;
import states.WatermelonState;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Watermelon extends Fruit {

    public Watermelon() {
        super();
        objectState = new WatermelonState();
        initialVelocity = 30;
        fallingVelocity = 40;
    }

    @Override
    public void moveUp() {
        Random r = new Random();
        int m = r.nextInt(5);
        setYLocation(YLocation - m);
        setXLocation(XLocation - m);
        if (YLocation < 20) {
            setMovingUp(false);
        }
    }

    @Override
    public void moveDown() {
        setYLocation(YLocation + getFallingVelocity());
        if ( YLocation > 600|| XLocation < 0) {
            setHasMovedOffScreen(true);
        }
    }

    @Override
    public void slice(Game game) {
        isSliced = true;
        objectState.slice();
    }

    @Override
    public void move(double time) {
    }

    @Override
    public Image[] getImages() {

        /*Image image = null;
        try {
            image = new Image(new FileInputStream("src\\images\\watermelon.png"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        images[0] = new ImageView(image);*/
        ImageIcon icon = new ImageIcon("src\\images\\watermelon.png");
        //images[0] = new ImageView(image);
        //images[0] = image;
        images[0] = icon.getImage();
        //images[0].setFitHeight(100);
        //images[0].setFitWidth(100);
        return images;
    }
}
