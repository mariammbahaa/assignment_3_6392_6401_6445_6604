package controller;


import java.awt.*;
import java.io.*;

public class FontClass {

    private static Font gameFont;
    private static Font gameOverFont;

    public static Font getGameFont() throws IOException, FontFormatException {
        String fName = "src//font.ttf";

        try {
            BufferedInputStream myStream = new BufferedInputStream(
                    new FileInputStream(fName));
            Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, myStream);
            gameFont = ttfBase.deriveFont(Font.PLAIN, 30);
            gameOverFont = ttfBase.deriveFont(Font.PLAIN, 100);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Font not loaded.");
        }

       return gameFont;
    }
    public static Font getGameOverFont()
    {
        String fName = "src//font.ttf";
        try {
            BufferedInputStream myStream = new BufferedInputStream(
                    new FileInputStream(fName));
            Font ttfBase = Font.createFont(Font.TRUETYPE_FONT, myStream);
            gameOverFont = ttfBase.deriveFont(Font.PLAIN, 100);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Font not loaded.");
        }
        return gameOverFont;
    }
}
