package states;

import game.Game;

import javax.swing.*;

public class PineappleState implements GameObjectState {

    @Override
    public void slice() {
        int currentScore = Game.getInstance().getCurrentScore();
        currentScore ++;
        Game.getInstance().sliceObjects();
        Game.getInstance().setCurrentScore(currentScore);
        Game.getInstance().bestScoreComparator();
    }

    @Override
    public ImageIcon[] objectState() {
        ImageIcon[] images = new ImageIcon[2];
        images[0] = new ImageIcon("src//images//pineappleCut1.png");
        images[1] = new ImageIcon("src//images//pineappleCut2.png");
        return images;
    }

}
