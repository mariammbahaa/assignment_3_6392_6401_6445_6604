package states;

import game.Game;

import javax.swing.*;

public class WatermelonState implements GameObjectState {

    @Override
    public void slice() {
        int currentScore = Game.getInstance().getCurrentScore();
        currentScore ++;
        Game.getInstance().sliceObjects();
        Game.getInstance().setCurrentScore(currentScore);
        Game.getInstance().bestScoreComparator();
    }

    @Override
    public ImageIcon[] objectState() {
        ImageIcon[] images = new ImageIcon[2];
        images[0] = new ImageIcon("src//images//watermelonCut1.png");
        images[1] = new ImageIcon("src//images//watermelonCut2.png");
        return images;
    }
}
