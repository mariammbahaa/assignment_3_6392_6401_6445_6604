import gameObjects.Fruit;
import gameObjects.GameObject;

import javax.xml.bind.JAXBException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

public class TimerActionListener implements ActionListener {

    GamePanel g;
    GameController gameController;
    static long milliseconds = System.currentTimeMillis();
    static int seconds = 0;
    static int minutes = 0;
    static boolean state=true;

    public TimerActionListener(GamePanel g) {
        this.g = g;
        gameController = GameController.getInstance();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        List<GameObject> gameObjects = g.gameObjects;
        int c = g.c++;
        for (int i = 0; i < gameObjects.size(); i++) {
            GameObject gameObject = gameObjects.get(i);
            if (gameObject.getMovingUp()) {
                gameObject.moveUp();
            } else {
                gameObject.moveDown();
            }
        }


        if(gameController.getLevel() > 0 && gameController.getLevel() <= 3) {
            g.levelLabel.setText("Level  " + gameController.getLevel());
            g.levelLabel.setVisible(true);
        }

        if(gameController.getCurrentScore() > 25) {

            /*try {
                gameController.resetGame();
            } catch (JAXBException e) {
                e.printStackTrace();
            }*/
            /*try {
                g.mainvieww.changePanelGameOver();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (FontFormatException e) {
                e.printStackTrace();
            }*/

            try {
                gameController.saveGame();
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            g.mainvieww.changePanelWinningPanel();
        }

        for (int i = 0; i < gameObjects.size(); i++) {
            GameObject gameObject = gameObjects.get(i);
            int livesCount = gameController.getLives();
            if (!gameObject.getSliced()) {
                if (gameObject instanceof Fruit) {
                    if (gameObject.getHasMovedOffScreen()) {
                    --livesCount;
                    gameController.setLives(livesCount);
                    }
                    /*if (livesCount == 0) {
                    try {
                        g.mainvieww.changePanelGameOver();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (FontFormatException ex) {
                        ex.printStackTrace();
                    }
                    try {
                        gameController.saveGame();
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    }
                    try {
                        gameController.resetGame();
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    }
                   }*/
                }
            }
        }

        boolean allOffScreen = true;
        for (int i = 0; i < gameObjects.size(); i++) {
            GameObject go = gameObjects.get(i);
            if (!go.getHasMovedOffScreen()) {
                allOffScreen = false;
                break;
            }
        }
        if (allOffScreen) {
            gameObjects.clear();
            g.gameObjects = gameController.createGameObjects(c);
        }

        if (gameController.getLives() == 3) {
            g.lives_label1.setVisible(true);
            g.lives_label1.validate();
            g.lives_label2.setVisible(true);
            g.lives_label2.validate();
            g.lives_label3.setVisible(true);
            g.lives_label3.validate();
        }
        else if (gameController.getLives() == 2) {
            g.lives_label1.setVisible(true);
            g.lives_label1.validate();
            g.lives_label2.setVisible(true);
            g.lives_label2.validate();
            g.lives_label3.setVisible(false);
            g.lives_label3.validate();
        }
        else if (gameController.getLives() == 1) {
            g.lives_label1.setVisible(true);
            g.lives_label1.validate();
            g.lives_label2.setVisible(false);
            g.lives_label2.validate();
            g.lives_label3.setVisible(false);
            g.lives_label3.validate();
        }
        if(gameController.getLives() == 0)
        {
            try {
                g.mainvieww.changePanelGameOver();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (FontFormatException e) {
                e.printStackTrace();
            }
            try {
                gameController.saveGame();
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            try {
                gameController.resetGame();
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        g.repaint();
    }
}
