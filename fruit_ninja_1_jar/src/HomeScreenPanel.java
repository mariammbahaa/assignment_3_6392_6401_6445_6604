import controller.FontClass;

import javax.swing.*;
import javax.xml.bind.JAXBException;
import java.awt.*;
import java.io.IOException;

public class HomeScreenPanel extends JPanel {

        GameController gameController;
        JLabel watermelonLabel;
        Mainvieww mainvieww;

        public HomeScreenPanel(Mainvieww mainvieww) throws JAXBException {
            this.mainvieww = mainvieww;
            setLayout(new BorderLayout());
            gameController = GameController.getInstance();
            watermelonLabel = new JLabel("New Game");
            setPreferredSize(new Dimension(1000, 600));
            watermelonLabel.setLocation(500,300);
            ImageIcon imageIcon = new ImageIcon("src//images//watermelon.png");
            Image image = imageIcon.getImage();
            Image newImage = image.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
            imageIcon = new ImageIcon(newImage);
            watermelonLabel.setIcon(imageIcon);
            watermelonLabel.setForeground(Color.white);
            try {
                watermelonLabel.setFont(FontClass.getGameFont());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (FontFormatException e) {
                e.printStackTrace();
            }
            watermelonLabel.addMouseMotionListener(new HomeScreenMouseMotionListener(watermelonLabel, mainvieww));
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            this.add(watermelonLabel,BorderLayout.SOUTH);
            watermelonLabel.setVisible(true);
            ImageIcon background = new ImageIcon("src//images//background.jpg");
            ImageIcon logo = new ImageIcon("src//images//logo1.png");
            Image  backgroundImage = background.getImage();
            Image logoImage = logo.getImage();
            g.drawImage(backgroundImage,0,0,null);
            g.drawImage(logoImage,100 , 20, 641, 363, null);
            this.validate();
            repaint();
        }


}
