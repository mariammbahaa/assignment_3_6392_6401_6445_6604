import commands.LoadGameCommand;
import commands.ResetGameCommand;
import commands.SaveGameCommand;
import game.CareTaker;
import game.Game;
import game.Levels;
import gameObjects.GameObject;

import javax.xml.bind.JAXBException;
import java.util.List;

public class GameController {

    private Game game;
    private LoadGameCommand loadGameCommand;
    private SaveGameCommand saveGameCommand;
    private ResetGameCommand resetGameCommand;

    private static GameController gameController = null;

    private GameController()
    {
       game = Game.getInstance();
       loadGameCommand = new LoadGameCommand();
       saveGameCommand = new SaveGameCommand();
       resetGameCommand = new ResetGameCommand();
    }

    public static GameController getInstance()
    {
      if( gameController == null)
      {
          gameController = new GameController();
      }
      return gameController;
    }

    public int getLevel() {
        return game.getLevel();
    }

    public void setLevel(int level) {
        game.setLevel(level);
    }

    public int getCurrentScore() {
        return game.getCurrentScore();
    }

    public void setCurrentScore(int currentScore) {
        game.setCurrentScore(currentScore);
    }

    public int getBestScore() {
        return game.getBestScore();
    }

    public void setBestScore(int bestScore) {
        game.setBestScore(bestScore);
    }

    public int getLives() {
        return game.getLives();
    }

    public void setLives(int lives) {
        game.setLives(lives);
    }

    public void bestScoreComparator()
    {
        game.bestScoreComparator();
    }

    public List<GameObject> createGameObjects(int c)
    {
        return Game.getInstance().createGameObjects(c);
    }

    public void saveGame() throws JAXBException {
       saveGameCommand.execute();
    }

    public void loadGame() throws JAXBException {
        loadGameCommand.execute();
    }

    public void resetGame() throws JAXBException {
        resetGameCommand.execute();
    }
}
