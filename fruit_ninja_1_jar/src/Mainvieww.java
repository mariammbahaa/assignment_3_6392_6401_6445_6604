import commands.Command;
import commands.LoadGameCommand;
import commands.ResetGameCommand;
import commands.SaveGameCommand;
import game.Game;
import game.Level;
import game.Levels;
import gameObjects.Apple;
import gameObjects.Pineapple;
import gameObjects.Watermelon;

import javax.swing.*;
import javax.xml.bind.JAXBException;
import java.awt.*;
import java.awt.event.MouseMotionListener;
import java.io.IOException;

public class Mainvieww extends JFrame {

    GamePanel gamePanel;
    GameOverPanel gameOverPanel;
    HomeScreenPanel homeScreenPanel;
    WinningPanel winningPanel;

    public Mainvieww() throws IOException, FontFormatException {
        super("Fruit Ninja");
        setSize(1000, 600);
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(new BorderLayout());
        try {
            homeScreenPanel = new HomeScreenPanel(this);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        add(homeScreenPanel, BorderLayout.CENTER);
        homeScreenPanel.setVisible(true);
    }

    public void changePanelGamePanel()
    {
        try {
            gamePanel = new GamePanel(this);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FontFormatException e) {
            e.printStackTrace();
        }
        this.add(gamePanel);
        gamePanel.setVisible(true);
        homeScreenPanel.setVisible(false);
        setContentPane(gamePanel);
        revalidate();
        homeScreenPanel.validate();
        invalidate();
        gamePanel.repaint();
        pack();
    }

    public void changePanelGameOver() throws IOException, FontFormatException {
        gameOverPanel = new GameOverPanel(this);
        this.add(gameOverPanel);
        gameOverPanel.setVisible(true);
        gamePanel.setVisible(false);
        setContentPane(gameOverPanel);
        revalidate();
        gamePanel.validate();
        invalidate();
        gameOverPanel.repaint();
        pack();
    }

    public void changePanelHomescreen()
    {
        try {
            homeScreenPanel = new HomeScreenPanel(this);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        this.add(homeScreenPanel);
        homeScreenPanel.setVisible(true);
        if(gameOverPanel!=null){
        gameOverPanel.setVisible(false);
        gameOverPanel.validate();
        }
        if(winningPanel!=null) {
            winningPanel.setVisible(false);
            winningPanel.validate();
        }
        setContentPane(homeScreenPanel);
        revalidate();
        invalidate();
        homeScreenPanel.repaint();
        pack();
    }

    public void changePanelWinningPanel()
    {
        try {
            winningPanel = new WinningPanel(this);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FontFormatException e) {
            e.printStackTrace();
        }
        this.add(winningPanel);
        winningPanel.setVisible(true);
        gamePanel.setVisible(false);
        setContentPane(winningPanel);
        revalidate();
        gamePanel.validate();
        invalidate();
        winningPanel.repaint();
        pack();
    }

    public static void main(String[] args) throws IOException, FontFormatException, JAXBException {
        Mainvieww frame = new Mainvieww();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
