package sounds;


import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.*;
//import jaco.mp3.player.MP3Player;
import java.io.FileInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FatalBombSound {

    private static Player playMP3;

    public static void playSound() throws JavaLayerException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src//sounds//Bomb.mp3");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        playMP3 = new Player(fis);
        playMP3.play();
    }
    public static void stopSound()
    {
        playMP3.close();
    }

}
