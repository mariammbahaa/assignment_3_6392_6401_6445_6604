package commands;

import game.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class LoadGameCommand implements Command {

    private Game game;
    private CareTaker careTaker;

    public LoadGameCommand()
    {
        game = Game.getInstance();
        careTaker = CareTaker.getInstance();
    }

    @Override
    public void execute() throws JAXBException {

        JAXBContext jaxbContextGameState = JAXBContext.newInstance(GameState.class);
        Unmarshaller unmarshallerGameState = jaxbContextGameState.createUnmarshaller();
        GameState gameState = (GameState) unmarshallerGameState.unmarshal( new File("src//files//game.xml"));

        JAXBContext jaxbContextLevels = JAXBContext.newInstance(Levels.class);
        Unmarshaller unmarshallerLevels = jaxbContextLevels.createUnmarshaller();
        Levels loadedLevels = (Levels) unmarshallerLevels.unmarshal(new File("src//files//levels.xml"));
        List<Level> levels = loadedLevels.getLevels();

        game.loadGame(gameState,levels);
        careTaker.addStateStack(gameState);
    }

}
