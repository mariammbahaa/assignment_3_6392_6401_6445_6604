package commands;

import game.CareTaker;
import game.Game;
import game.GameState;
import game.GameStates;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SaveGameCommand implements Command {

    private Game game;
    private GameState gameState;
    private CareTaker careTaker;

    public SaveGameCommand()
    {
        game = Game.getInstance();
        careTaker = CareTaker.getInstance();
    }

    @Override
    public void execute() throws JAXBException {

        gameState = game.saveGame();
        careTaker.addStateStack(gameState);

        List<GameState> gameStates = new ArrayList<>();
        for(int i =0; i < careTaker.getGameStateStack().size(); i++)
        {
            GameState gameState = careTaker.getStateList(i);
            gameStates.add(gameState);
        }

        JAXBContext jaxbContext = JAXBContext.newInstance(GameState.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        GameState savingGameState = new GameState(game.getLevel(), game.getCurrentScore(), game.getBestScore(),gameState.getLives());
        marshaller.marshal(savingGameState, new File("src//files//game.xml"));

    }

}
