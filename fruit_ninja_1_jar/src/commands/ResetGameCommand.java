package commands;

import game.Game;

import javax.xml.bind.JAXBException;

public class ResetGameCommand implements Command{

    private Game game;

    public ResetGameCommand()
    {
        game = Game.getInstance();
    }

    @Override
    public void execute() throws JAXBException {
        game.resetGame();
    }
}
